import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    type: String,
    description: '用户账号',
    example: 'sumaolin',
  })
  @IsNotEmpty()
  readonly username: string;

  @ApiProperty({
    type: String,
    description: '密码',
    example: '123456',
  })
  @IsNotEmpty({
    message: '密码不能为空',
  })
  readonly password: string;

  @ApiProperty({
    type: String,
    description: '用户邮箱',
    example: 'sumaolin@qq.com',
  })
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({
    type: String,
    description: '用户昵称',
    example: 'Kevin Su',
  })
  @IsNotEmpty()
  readonly nickname: string;
}
