import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  create(createUserDto: CreateUserDto) {
    const user = new UserEntity();
    (user.username = createUserDto.username),
      (user.password = createUserDto.password),
      (user.email = createUserDto.email),
      (user.nickname = createUserDto.nickname);

    console.log('This action adds a new user');

    return this.userRepository.save(user);
  }

  findAll() {
    return this.userRepository.find();
  }

  findOne(id: string) {
    // return `This action returns a #${id} user`;
    return this.userRepository.findByIds([id]);
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  async remove(id: string) {
    // return `This action removes a #${id} user`;
    const user = await this.userRepository.findOne(id);

    return this.userRepository.remove(user);
  }
}
