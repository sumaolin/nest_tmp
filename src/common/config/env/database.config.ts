// env configuration
import { registerAs } from '@nestjs/config';
export interface EnvDatabaseOptions {
  port: number; // 数据库端口
  host: string; // 数据库地址
  user: string; //用户名
  password: string; // 密码
  database: string; // 库名
  connectionLimit: number; // 连接限制
}
export default registerAs(
  'EnvDatabaseOptions',
  (): EnvDatabaseOptions => ({
    port: parseInt(process.env.DB_PORT, 10), // 数据库端口
    host: process.env.DB_HOST, // 数据库地址
    user: process.env.DB_USER, //用户名
    password: process.env.DB_PWA, // 密码
    database: process.env.DB_DATABASE, // 库名
    connectionLimit: parseInt(process.env.DB_CONNECTION_LIMIT, 10), // 连接限制
  }),
);
