// env configuration
import { registerAs } from '@nestjs/config';
export interface EnvHostOptions {
  port: number; // 服务器端口
  host: string; // 服务器地址
}
export default registerAs(
  'EnvHostOptions',
  (): EnvHostOptions => ({
    port: parseInt(process.env.SERVE_LISTENER_PORT, 10), // 服务器端口
    host: process.env.DB_HOST, // 服务器地址
  }),
);
