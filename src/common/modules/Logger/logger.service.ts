import { Logger as AppLogger } from '@nestjs/common';
import { Logger } from './log4js.util';

export class LoggerService extends AppLogger {
  log(message: any, trace: string) {
    // super.log(message, trace);
    Logger.info(trace, message);
  }

  error(message: any, trace: string) {
    // super.error(message, trace);
    Logger.error(trace, message);
  }

  warn(message: any, trace: string) {
    // super.warn(message, trace);
    Logger.warn(trace, message);
  }

  debug(message: any, trace: string) {
    // super.debug(message, trace);
    Logger.debug(trace, message);
  }

  verbose(message: any, trace: string) {
    // super.verbose(message, trace);
    Logger.info(trace, message);
  }

  access(message: any) {
    Logger.access(message);
  }
}
